document.addEventListener("DOMContentLoaded", async () => {
    const monstersEl = document.querySelector(".monsters");
    const prefix = "mhrisesnap_";
    for (const monster of monsters) {
        const buttonEl = document.createElement("button");
        const imgEl = document.createElement("img");
        imgEl.src = monster.image;
        imgEl.alt = "";
        const labelEl = document.createElement("span");
        labelEl.appendChild(document.createTextNode(monster.name));
        buttonEl.appendChild(imgEl);
        buttonEl.appendChild(labelEl);
        if (has(monster.name)) {
            buttonEl.classList.add("has");
        }
        buttonEl.type = "button";
        buttonEl.addEventListener("click", () => toggle(monster.name, buttonEl));
        buttonEl.addEventListener("dragstart", e => e.preventDefault());
        monstersEl.appendChild(buttonEl);
    }

    document.getElementById("filter").addEventListener("change", e =>
        monstersEl.setAttribute("data-filter", e.target.value));

    function has(name) {
        return localStorage[prefix + name] != null;
    }
    function toggle(name, buttonEl) {
        if (has(name)) {
            delete localStorage[prefix + name];
            buttonEl.classList.remove("has");
        } else {
            localStorage[prefix + name] = 1;
            buttonEl.classList.add("has");
        }
    }
});
