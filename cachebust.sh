#!/bin/sh
set -e
dt="$(date +%s)"
sed -Ei "s/\?v=[0-9]+/?v=$dt/" index.html
