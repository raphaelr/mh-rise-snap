// fuck node and fuck modern web development
const https = require("https");
const jsdom = require("jsdom");
const fs = require("fs");

https.get("https://mhrise.kiranico.com/data/monsters", res => {
    if (res.statusCode !== 200) throw new Error("panic");
    res.setEncoding("utf-8");
    let s = "";
    res.on("data", x => s += x);
    res.on("end", async () => {
        const document = new jsdom.JSDOM(s).window.document;
        const pics = document.querySelectorAll("a[href*='/monsters/'] img");
        const xs = [];
        for (const pic of pics) {
            const name = pic.getAttribute("alt");
            const srcUrl = pic.getAttribute("src");
            const dstPath = `../images/${name}.png`;
            if (!exists(dstPath)) {
                console.log("Download", srcUrl, "to", dstPath);
                const file = fs.createWriteStream(dstPath);
                await new Promise(resolve => {
                    https.get(srcUrl, res => {
                        if (res.statusCode !== 200) throw new Error("panic");
                        res.pipe(file);
                        res.on("end", resolve);
                    });
                });
            }
            xs.push({
                name,
                image: dstPath.substr(3),
            });
        }
        // Serialize like this for easy diffing
        const d = xs.map(x => JSON.stringify(x)).join(",\n");
        fs.writeFileSync("../data.js", `var monsters = [\n${d}\n];`);
    });
});

function exists(p) {
    try {
        return !!fs.statSync(p);
    } catch (e) {
        return false;
    }
}
