#!/bin/sh
set -e
. ./deploy.sh.local
rsync -rP --exclude=generate_data --exclude=deploy.sh.local ./ "$dst"
